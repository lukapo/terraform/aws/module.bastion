# BASTION MODULE

## Input Variables
* `additional_user_data_script` (required): Additional shell script to run as a part of userdata for bastion instance
* `allowed_cidr` (default `["0.0.0.0/0"]`): A list of CIDR Networks to allow ssh access to.
* `ami_id` (required): AMI ID to use for Bastion instance
* `apply_changes_immediately` (required): Should be false to not apply the changes at once and prevent recreation of auto-scaling group
* `associate_public_ip_address` (required): Should be false to prevent needless association of a public IP due to EIP usage; this may be overriden by subnet settings
* `aws_metadata_ip` (default `"169.254.169.254"`): AWS EC2 metadata IP
* `bastion_user_data_file` (default `"bastion_user_data.sh.tpl"`): Filepath of the userdata script template file
* `enable_monitoring` (required): Should be false to not enable detailed monitoring, as it is not necessary for the bastion host
* `extra_tags` (required): A list of tags to associate to the bastion instance.
* `instance_type` (required): Bastion instance type (recommend: t2.micro or t2.small)
* `instance_volume_size_gb` (default `30`): The root volume size, in gigabytes
* `key_name` (required): Name of the ssh key to use for the instances default user
* `name` (required): Base name to use for all the resources
* `region` (required): AWS region to deploy the bastion in. It is used for AWS CLI calls from within Bastion host (userdata)
* `security_group_ids` (required): Comma seperated list of security groups to apply to the bastion.
* `ssh_user` (default `"lukapo"`): Ssh user for the instance (ubuntu (ubuntu), ec2-user (amz linux), admin (debian))
* `subnet_ids` (required): A list of subnet ids
* `vpc_id` (required): ID of the VPC to deploy into

## Output Values
* `asg_id`
* `security_group_id`
* `ssh_user`

## Managed Resources
* `aws_autoscaling_group.bastion` from `aws`
* `aws_iam_instance_profile.bastion` from `aws`
* `aws_iam_role.bastion` from `aws`
* `aws_iam_role_policy.bastion` from `aws`
* `aws_iam_role_policy_attachment.s3-policy-attachment` from `aws`
* `aws_launch_configuration.bastion` from `aws`
* `aws_security_group.bastion` from `aws`
* `aws_security_group_rule.bastion_all_egress` from `aws`
* `aws_security_group_rule.ssh_ingress` from `aws`

## Data Resources
* `data.aws_iam_policy_document.bastion` from `aws`
* `data.aws_iam_policy_document.bastion-role-policy` from `aws`
* `data.template_file.bastion_user_data` from `template`
