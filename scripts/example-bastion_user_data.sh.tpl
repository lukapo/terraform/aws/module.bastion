#!/bin/bash

cd /root

echo "USERDATA: Deploying Lukapo keys..."
curl https://deploy-server.s3-eu-west-1.amazonaws.com/latest/deploy_centos_standard_settings.sh > deploy_centos_standard_settings.sh;
bash deploy_centos_standard_settings.sh

echo "lukapo ALL = NOPASSWD: ALL" > /etc/sudoers.d/lukapo
echo "lukapo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/lukapo

echo "USERDATA: Setup company dev enviroment access"

useradd company-dev

echo "company-dev ALL = NOPASSWD: ALL" > /etc/sudoers.d/company-dev
echo "company-dev ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/company-dev

mkdir /home/company-dev/.ssh
#echo "ssh-rsa example ssh-public-key" >> /home/company-dev/.ssh/authorized_keys

chown -R company-dev:company-dev /home/company-dev/
chmod 700 /home/company-dev/.ssh
chmod 600 /home/company-dev/.ssh/authorized_keys

echo "USERDATA: Installing pip and aws cli"

curl -O https://bootstrap.pypa.io/get-pip.py
python get-pip.py --user

echo 'export PATH=~/.local/bin:$PATH' >> ~/.bashrc
source ~/.bashrc

pip install awscli --upgrade --user

sudo yum -y install jq
eval $(
  curl http://${AWS_METADATA_IP}/latest/meta-data/iam/security-credentials/${EC2_BASTION_ROLE} \
    | jq -r '
     "export AWS_ACCESS_KEY_ID=\(.AccessKeyId) ;
      export AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey) ;
      export AWS_SESSION_TOKEN=\(.Token)"
    '
)

ec2_bastion_hostname=$(curl 169.254.169.254/latest/meta-data/public-hostname)

echo "USERDATA: Associating DNS record (${ROUTE53_CNAME_SUBDOMAIN}.${ROUTE53_DOMAIN}) to instance hostname ($${ec2_bastion_hostname})..."

echo '{
            "Comment": "Upsert a CNAME record for Bastion instance in an ASG",
            "Changes": [{
            "Action": "UPSERT",
                        "ResourceRecordSet": {
                                    "Name": "${ROUTE53_CNAME_SUBDOMAIN}.${ROUTE53_DOMAIN}",
                                    "Type": "CNAME",
                                    "TTL": 300,
                                 "ResourceRecords": [{ "Value": "'"$${ec2_bastion_hostname}"'" }]
}}]
}' > /tmp/cname_batch.json

aws route53 change-resource-record-sets --region "${AWS_REGION}" --change-batch file:///tmp/cname_batch.json --hosted-zone-id "${ROUTE53_TOOLS_ZONE_ID}"

echo "USERDATA: Running additional commands script..."
${ADDITIONAL_USER_DATA_SCRIPT}

echo "USERDATA: Finished."

