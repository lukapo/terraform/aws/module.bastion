variable "allowed_cidr" {
  description = "A list of CIDR Networks to allow ssh access to."
  type        = list(string)

  default = [
    "0.0.0.0/0",
  ]
}

variable "name" {
  description = "Base name to use for all the resources"
  type        = string
}

variable "extra_tags" {
  description = "A list of tags to associate to the bastion instance."
  type        = list(string)
  default     = []
}

variable "ami_id" {
  description = "AMI ID to use for Bastion instance"
  type        = string
}

variable "instance_type" {
  description = "Bastion instance type (recommend: t2.micro or t2.small)"
  type        = string
}

variable "instance_volume_size_gb" {
  description = "The root volume size, in gigabytes"
  type        = number
  default     = 30
}

variable "bastion_user_data_file" {
  description = "Filepath of the userdata script template file"
  type        = string
  default     = "bastion_user_data.sh.tpl"
}

variable "enable_monitoring" {
  description = "Should be false to not enable detailed monitoring, as it is not necessary for the bastion host"
  type        = bool
  default     = false
}

variable "ssh_user" {
  description = "Ssh user for the instance (ubuntu (ubuntu), ec2-user (amz linux), admin (debian))"
  type        = string
  default     = "lukapo"
}

variable "additional_user_data_script" {
  description = "Additional shell script to run as a part of userdata for bastion instance"
  type        = string
  default     = ""
}

variable "region" {
  description = "AWS region to deploy the bastion in. It is used for AWS CLI calls from within Bastion host (userdata)"
  type        = string
}

variable "aws_metadata_ip" {
  description = "AWS EC2 metadata IP"
  type        = string
  default     = "169.254.169.254"
}

variable "vpc_id" {
  description = "ID of the VPC to deploy into"
  type        = string
}

variable "security_group_ids" {
  description = "Comma seperated list of security groups to apply to the bastion."
  type        = string
  default     = ""
}

variable "subnet_ids" {
  description = "A list of subnet ids"
  type        = list(string)
  default     = []
}

variable "associate_public_ip_address" {
  description = "Should be false to prevent needless association of a public IP due to EIP usage; this may be overriden by subnet settings"
  type        = bool
  default     = false
}

variable "key_name" {
  description = "Name of the ssh key to use for the instances default user"
  type        = string
}

variable "apply_changes_immediately" {
  description = "Should be false to not apply the changes at once and prevent recreation of auto-scaling group"
  type        = bool
  default     = false
}

variable "route53_domain" {
  description = "Domain name to use in Route53 to add a record for Bastion host from within its own userdata"
  type        = string
}

variable "route53_cname_subdomain" {
  description = "Full domain address of the CNAME that will point to the Bastion host EC2 public domain"
  type        = string
}

variable "route53_tools_zone_id" {
  description = "ID of the hosted zone bastion CNAME is to be created under"
  type        = string
}

