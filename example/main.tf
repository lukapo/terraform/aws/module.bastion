provider "aws" {
  region = "eu-central-1"
}

data "aws_vpc" "main" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.main.id
}

data "aws_ami" "amazon_linux_ecs" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-*-x86_64-ebs"]
  }

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
}


resource "aws_key_pair" "ssh-public-key" {
  key_name   = local.key_name
  public_key = local.key_public
}

resource "aws_eip" "bastion-eip" {
  vpc = true

  tags = {
    Name = "eip-${local.env_project}-bastion"
  }
}

locals {
  project                = "test"
  environment            = "dev"
  env_project            = "${local.environment}-${local.project}"
  bastion_user-data_file = "../scripts/example-bastion_user_data.sh.tpl"
  key_name               = "test"
  aws_region             = "eu-central-1"
  key_public             = "ssh public key here.."
  tools_domain           = "test.me"
  tools_zone_id          = "Z1KN75EWQP081T"
}

module "bastion" {
  source = "../"

  vpc_id                  = data.aws_vpc.main.id
  name                    = local.env_project
  region                  = local.aws_region
  subnet_ids              = data.aws_subnet_ids.all.ids
  instance_type           = "t2.medium"
  ami_id                  = data.aws_ami.amazon_linux_ecs.id
  key_name                = local.key_name
  route53_domain          = local.tools_domain
  route53_tools_zone_id   = local.tools_zone_id
  route53_cname_subdomain = "bastion.${local.environment}"
  bastion_user_data_file  = local.bastion_user-data_file
}