resource "aws_iam_instance_profile" "bastion" {
  name = "${var.name}-bastion"
  role = aws_iam_role.bastion.name
}

data "aws_iam_policy_document" "bastion-role-policy" {
  statement {
    sid    = ""
    effect = "Allow"

    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com",
        "cloudtrail.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role" "bastion" {
  name               = "${var.name}-bastion-role"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.bastion-role-policy.json
}

data "aws_iam_policy_document" "bastion" {
  statement {
    sid = "ec2"

    effect = "Allow"

    actions = [
      "autoscaling:Describe*",
      "ec2:Describe*",
      "ec2:AssociateAddress",
      "ecs:*",
      "cloudwatch:*",
      "logs:*",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    sid = "route53"

    effect = "Allow"

    actions = [
      "route53:ChangeResourceRecordSets",
    ]

    resources = [
      "arn:aws:route53:::hostedzone/${var.route53_tools_zone_id}",
    ]
  }

}

resource "aws_iam_role_policy" "bastion" {
  name   = "rolepolicy-${var.name}-bastion"
  role   = aws_iam_role.bastion.id
  policy = data.aws_iam_policy_document.bastion.json
}

resource "aws_iam_role_policy_attachment" "s3-policy-attachment" {
  role       = aws_iam_role.bastion.id
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}


