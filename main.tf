resource "aws_security_group" "bastion" {
  name        = "security_group-${var.name}-bastion"
  vpc_id      = var.vpc_id
  description = "Bastion security group (only SSH inbound access is allowed)"

  tags = {
    Name = "sg-${var.name}-bastion"
  }
}

resource "aws_security_group_rule" "ssh_ingress" {
  type              = "ingress"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  cidr_blocks       = var.allowed_cidr
  security_group_id = aws_security_group.bastion.id
}

resource "aws_security_group_rule" "bastion_all_egress" {
  type      = "egress"
  from_port = "0"
  to_port   = "65535"
  protocol  = "all"

  cidr_blocks = [
    "0.0.0.0/0",
  ]

  ipv6_cidr_blocks = [
    "::/0",
  ]

  security_group_id = aws_security_group.bastion.id
}

data "template_file" "bastion_user_data" {
  template = file("../templates/${var.bastion_user_data_file}")

  vars = {
    AWS_REGION                  = var.region
    AWS_METADATA_IP             = var.aws_metadata_ip
    EC2_BASTION_ROLE            = aws_iam_role.bastion.id
    ROUTE53_DOMAIN              = var.route53_domain
    ROUTE53_CNAME_SUBDOMAIN     = var.route53_cname_subdomain
    ROUTE53_TOOLS_ZONE_ID       = var.route53_tools_zone_id
    ADDITIONAL_USER_DATA_SCRIPT = var.additional_user_data_script
  }
}

resource "aws_launch_configuration" "bastion" {
  name_prefix       = "launchconf-${var.name}-bastion-"
  image_id          = var.ami_id
  instance_type     = var.instance_type
  user_data         = data.template_file.bastion_user_data.rendered
  enable_monitoring = var.enable_monitoring

  security_groups = compact(concat(list(aws_security_group.bastion.id), split(",", var.security_group_ids)))

  root_block_device {
    volume_size = var.instance_volume_size_gb
  }

  iam_instance_profile        = aws_iam_instance_profile.bastion.id
  associate_public_ip_address = var.associate_public_ip_address
  key_name                    = var.key_name

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "bastion" {
  name = "asg-${var.apply_changes_immediately ? aws_launch_configuration.bastion.name : var.name}-bastion"

  vpc_zone_identifier = var.subnet_ids

  desired_capacity          = "1"
  min_size                  = "1"
  max_size                  = "1"
  health_check_grace_period = "60"
  health_check_type         = "EC2"
  force_delete              = false
  wait_for_capacity_timeout = 0
  launch_configuration      = aws_launch_configuration.bastion.name

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]

  tags = [
    {
      key                 = "Name"
      value               = "${var.name}-bastion"
      propagate_at_launch = true
    },
  ]

  lifecycle {
    create_before_destroy = true
  }
}
